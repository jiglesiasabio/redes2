/*
redes 2: práctica
Septiembre 2012

Javier Iglesia Sabio

100059087@alumnos.uc3m.es
javier.iglesia.sabio@gmail.com
*/

#include <iostream> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <pthread.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/stat.h>

using namespace std;
void error(const char *err);
void sendMsg(string msg);
void privateMsg(string receiver, string msg);
void printResponse(char *msg);
void printChannelResponse(char *msg);
char* getParam(char *buffer);
int isChannelMsg(char *msg);
void *receiver(void *arg);
void *DCCSender(void *arg);
void *DCCReceiver(void *arg);
void pong(char *param);
void ping(char *param);
int nick(const char *nick);
void user();
void quit(char *str);
void part(char *leave_message);
int isPing(char *buffer);
char* getPingParam(char *buffer);
int isUserCommand(char *line);
char* getUserCommand(char *line);
struct dcc_send_info proc_dcc_offer_line(char *line);
struct dcc_recv_info proc_dcc_download_line(char *line);
void who(char *param);
void list(char *channel);
void join(char *chan);
void DCCRequest(string filename, string ip, string port_,string receiver);
void comando_sleep(string param);
void irc_connect(string host,int port);
int isRrqCorrecto(string buffer, string filename);
int isDCCSend(char *msg);
int isACKCorrecto(uint16_t ack);

//Struct que se emplea para pasar la información requerida por el hilo DCCReceiver
struct dcc_recv_info{
	char ip[100];	
	char port[100];
	char filename[256];
	long int size;
};
//Struct que se emplea para pasar la información requerida por el hilo DCCSender
struct dcc_send_info{
	char username[100];
	char filename[256];
	int socket;
};

#define TRUE 1
#define FALSE 0
#define NICK 10
#define USER 11
#define QUIT 12
#define PART 13
#define LIST 14
#define WHO 15
#define PING 16
#define PONG 17
#define JOIN 18
#define OFFER 19
#define DOWNLOAD 20
#define MSG 21
#define HELP 22
#define AUTH 23
#define NOP 24
#define SLEEP 25
#define CONNECT 26
#define DISCONNECT 27
#define STATUS 100
#define SENDER 200
#define RECEIVER 300
int socketDesc,port,joined,_exit_,connected;
struct sockaddr_in serverAddr,my;
struct hostent *server;
pthread_mutex_t mutex_send,mutex_receive;
char user_command[200];
char channel[100];
string nickname;
pthread_t th_dcc_receiver;
pthread_t th_dcc_sender;
bool paramDepuracion = false;
string DCCFilename = "";
string mi_ip = "";
int sd;//Descriptor del socket usado para transferencia de ficheros

string puertos_ips[500];
string filenames[500];
long int sizes[500];

//---------------------------------------------------------------------------------
/*

##     ##    ###    #### ##    ## 
###   ###   ## ##    ##  ###   ## 
#### ####  ##   ##   ##  ####  ## 
## ### ## ##     ##  ##  ## ## ## 
##     ## #########  ##  ##  #### 
##     ## ##     ##  ##  ##   ### 
##     ## ##     ## #### ##    ##

*/
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
int main(int argc, char* argv[]) { 

	int it=0;
	while(it<500){
		puertos_ips[it]="";
		filenames[it]="";
		sizes[it]=0;
		it++;
	}
	
	strcpy(channel,"");
	joined=0;	
	_exit_=0;
	pthread_mutex_init(&mutex_send, NULL);
	pthread_mutex_init(&mutex_receive, NULL);	
	char paramServerPort[50] = "";
	char paramNickname[50] = "";
	char paramChannel[50] = "";
	char host[30]="";
	nickname = "";
	connected = 0;
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------	
	/*
		Parámetros:
			-s server:port
			-n nickname
			-c channel
			-d modo depuración
	*/

	if(argc == 1)
	{
		//No se recibieron parámetros por línea de comandos, se deben pedir de modo interactivo
		cout << "*** No se recibieron parámetros" << endl;
	}
	else
	{
		//se ha recibido algún parámetro ( o todos)
		for(int i = 0; i < argc; i++)
		{			
			if(strcmp (argv[i],"-s")==0)
			{
				//-s server:port
				if(argv[i+1] != NULL)
				{				
					strcpy ( paramServerPort, argv[i+1]);
				}				
				//control del formato del parámetro (host:port)
				char* p;
				if(strcmp(paramServerPort,"")!=0)
				{
					p = strtok(paramServerPort,":");
					if(p!=NULL)
					{
						strcpy(host,p);
						p = strtok(NULL,":");
						if(p!=NULL)
						{
							port = atoi(p);
						}
						else
						{
							cout << "*** falta puerto!" << endl;
						}
					}
					else
					{
						cout << "*** falta host!" << endl;
					}
				}
				else
				{
					cout << "*** uso: /connect host:port" << endl;
				}


			}
			else if(strcmp (argv[i],"-n")==0)
			{
				//-n nickname	
				if(argv[i+1]!=NULL)
				{			
					strcpy ( paramNickname, argv[i+1]);
				}
				else
				{
					error("ERROR-faltan parámetros");
				}
			}
			else if(strcmp (argv[i],"-c")==0)
			{
				//-c channel				
				if(argv[i+1]!=NULL)
				{			
					strcpy ( paramChannel, argv[i+1]);
				}
				else
				{
					error("ERROR-faltan parámetros");
				}
				//paramChannel = &argv[i+1];
			}
			else if(strcmp (argv[i],"-d")==0)
			{
				//-d (modo depuración)
				paramDepuracion = true;
			}	
		}//for
	}//else
	
	//Pedir interactivamente el Modo de depuración
	if(!paramDepuracion)
	{
		//pedir interactivamente paramDepuracion
		char c='x';
		while(!(c=='y' || c=='n'))
		{
			cout << "¿Deseea activar el modo de Depuración? [y/n]" << endl;
			cin  >> c;
		}
		if(c=='y')
		{
			paramDepuracion = true;
		}
	}
	/*
		---------------------------------------------------
		HACER LO QUE SE PUEDA CON LOS PARAMETROS RECIBIDOS
		---------------------------------------------------
		1.-Si se recibió 'host:port' se hace 'connect'
		2.-Si se recibió 'nick' se hace 'auth'
		3.-Si se recibió 'canal' de hace 'join'
	*/
	//IF SERVIDOR:PUERTO --> CONNECT
	if(strcmp(host,"")!=0 && port)
	{
		cout << "*** Conectando al servidor <" << host << "> en el puerto " << port << endl;
		irc_connect(host,port);		
		//IF NICK --> AUTH
		if(strcmp(paramNickname,"")!=0)
		{
			cout << "*** Registrando (auth) el nick <" << paramNickname << ">" << endl;
			//auth
			nick(paramNickname);
			user();			
			//IF CHANNEL --> JOIN
			if(strcmp(paramChannel,"")!=0)
			{
				cout << "*** Haciendo join al canal <" << paramChannel << ">" << endl;
				join(paramChannel);
			}
			else
			{
				cout << "*** No se proporcionó un canal. Emplee el comando 'join'." << endl;
			}
		}
		else
		{
			cout << "*** No se proporcionó un nick. Emplee el comando 'auth'." << endl;
		}
	}
	else
	{
		cout << "*** No se proporcionó un servidor:puerto. Emplee el comando 'connect'." << endl;
	}


	//Se lanza el Thread 'receiver' (encargado de escuchar todo loq ue llegue por el socket conectado al server IRC)
	pthread_t th_receiver;
	pthread_create( &th_receiver, NULL, receiver,NULL);	


	//BUCLE PRINCIPAL DE LA APLICACIÓN
	//Este bucle recoge la entrada del usuario y actúa en consecuencia
	char msg[100] = "";
	char msg_aux[100] = "";
	while(!_exit_)
	{
		cin.getline (msg,100);
		if(isUserCommand(msg))
		{
			//la línea recibida es un comando del usuario
			strcpy(msg_aux,msg);
			
			char comando[100] = "";
			strcpy(comando,getUserCommand(msg_aux));			
			if(paramDepuracion)
			{
				cout << "DDD ##########################################"<< endl;	
				cout << "DDD COMANDO "<< comando << endl;
				cout << "DDD MSG"<< msg << endl;
				cout << "DDD ##########################################"<< endl;	
			}

			int comandoInt = -1;

			if(strcmp(comando,"NICK")==0 || strcmp(comando,"nick")==0)
			{
				comandoInt = NICK;
			}
			else if(strcmp(comando,"USER")==0 || strcmp(comando,"user")==0)
			{
				comandoInt = USER;
			}
			else if(strcmp(comando,"QUIT")==0 || strcmp(comando,"quit")==0)
			{
				comandoInt = QUIT;
			}
			else if(strcmp(comando,"PART")==0 || strcmp(comando,"part")==0)
			{
				comandoInt = PART;
			}
			else if(strcmp(comando,"LIST")==0 || strcmp(comando,"list")==0)
			{
				comandoInt = LIST;
			}
			else if(strcmp(comando,"WHO")==0 || strcmp(comando,"who")==0)
			{
				comandoInt = WHO;
			}
			else if(strcmp(comando,"PING")==0 || strcmp(comando,"ping")==0)
			{
				comandoInt = PING;
			}
			else if(strcmp(comando,"PONG")==0 || strcmp(comando,"pong")==0)
			{
				comandoInt = PONG;
			}
			else if(strcmp(comando,"JOIN")==0 || strcmp(comando,"join")==0)
			{
				comandoInt = JOIN;
			}
			else if(strcmp(comando,"STATUS")==0 || strcmp(comando,"status")==0)
			{
				comandoInt = STATUS;
			}
			else if(strcmp(comando,"OFFER")==0 || strcmp(comando,"offer")==0)
			{
				comandoInt = OFFER;
			}
			else if(strcmp(comando,"DOWNLOAD")==0 || strcmp(comando,"download")==0)
			{
				comandoInt = DOWNLOAD;
			}
			else if(strcmp(comando,"MSG")==0 || strcmp(comando,"msg")==0)
			{
				comandoInt = MSG;
			}
			else if(strcmp(comando,"AUTH")==0 || strcmp(comando,"auth")==0)
			{
				comandoInt = AUTH;
			}
			else if(strcmp(comando,"HELP")==0 || strcmp(comando,"help")==0)
			{
				comandoInt = HELP;
			}
			else if(strcmp(comando,"NOP")==0 || strcmp(comando,"nop")==0)
			{
				comandoInt = NOP;
			}
			else if(strcmp(comando,"SLEEP")==0 || strcmp(comando,"sleep")==0)
			{
				comandoInt = SLEEP;
			}
			else if(strcmp(comando,"CONNECT")==0 || strcmp(comando,"connect")==0)
			{
				comandoInt = CONNECT;
			}
			else if(strcmp(comando,"DISCONNECT")==0 || strcmp(comando,"disconnect")==0)
			{
				comandoInt = DISCONNECT;
			}

			if(paramDepuracion)
			{
				cout << "DDD comandoInt -> " << comandoInt << endl;
			}			
			/*
				switch sobre el nombre del comando recibido	
				si el comando requiere parámetros se obtienen con una función específica			
				una vez tenemos el comando completo --> EJECUTAR COMANDO
			*/
			char param[100] ="";
			strcpy(param,getParam(msg));

			switch(comandoInt)
			{
				case NICK: //(3.1.2)
				{	
					if(paramDepuracion)
					{
						cout << "DDD EJECUTAR COMANDO - NICK" << endl;
						cout << "DDD PARAMS = " << param << endl;
					}
					nick(param);
					break;
				}
				case USER: //(3.1.3)
				{
					if(paramDepuracion)
					{
						cout << "DDD EJECUTAR COMANDO - USER" << endl;
						cout << "DDD PARAMS = " << param << endl;
					}
					user();
					break;
				}
				case QUIT: // (3.1.7)
				{
					close(socketDesc);
					exit(0);
					break;
				}
				case JOIN: // (3.2.1)
				{
					if(paramDepuracion)
					{
						cout << "DDD EJECUTAR COMANDO - JOIN" << endl;						
						cout << "DDD PARAMS = " << param << endl;
					}
					join(param);
					break;
				}
				case PART: //(3.2.2)
				{
					//dado que solo podemos hacer JOIN a un canal a la vez el comando part solo acepta un parámetro, el msg de salida
					//esto contradice la especificación del RFC, pero se adapta más al enunciado de la práctica, no tiene sentido tener que indicar el canal.
					if(paramDepuracion)
					{
						cout << "DDD EJECUTAR COMANDO - PART" << endl;						
						cout << "DDD PARAMS = " << param << endl;
					}
					part(param);
					break;
				}
				case LIST: //(3.2.6)
				{
					if(paramDepuracion)
					{
						cout << "DDD EJECUTAR COMANDO - LIST" << endl;						
						cout << "DDD PARAMS = " << param << endl;
					}
					list(param);
					break;
				}
				case WHO: //(3.6.1)
				{
					if(paramDepuracion)
					{
						cout << "DDD EJECUTAR COMANDO - WHO" << endl;						
						cout << "DDD PARAMS = " << param << endl;
					}
					who(param);
					break;
				}
				case PING: //(3.7.1)
				{
					if(paramDepuracion)
					{
						cout << "DDD EJECUTAR COMANDO - PING" << endl;						
						cout << "DDD PARAMS = " << param << endl;
					}
					ping(param);
					break;
				}
				case PONG: //(3.7.3)
				{
					if(paramDepuracion){
						cout << "DDD EJECUTAR COMANDO - PONG" << endl;						
						cout << "DDD PARAMS = " << param << endl;
					}
					pong(param);
					break;
				}
				case STATUS:
				{
					if(paramDepuracion)
					{
						cout << "DDD ###########################################################" << endl;
						cout << "DDD channel =" << channel << endl;
						cout << "DDD joined =" << joined << endl;
						cout << "DDD ###########################################################" << endl;
					}
					else
					{
						cout << "*** El comando STATUS sólo está disponible en modo DEBUG." << endl;
					}
					break;
				}
				case OFFER:
				{	
					//SOCKET UDP PARA TX FICHEROS
					struct sockaddr_in Server_Address; 	// Address Of Server
					// crear socket UDP

					int socketTX;
					
					socketTX = socket ( AF_INET, SOCK_DGRAM, 0 );
					if ( socketTX == -1 )
					{
				            error("No se pudo crear el socket!\n"); 	
					}
					int port = 8306; 
					Server_Address.sin_family = AF_INET;
				    	Server_Address.sin_port = htons(port);
				    	Server_Address.sin_addr.s_addr = INADDR_ANY;				
					while(bind ( socketTX, (struct sockaddr*)&Server_Address, sizeof(Server_Address))!=0)
					{
						cout << "*** El puerto " << port << " está ocupado, probando puerto " << port+1 << endl; 
						port++;
						Server_Address.sin_port = htons(port);
					}
					cout << "*** La tranferencia se realizará sobre el puerto - " << port << endl;				    
					dcc_send_info info_send;
					info_send = proc_dcc_offer_line(param);
					info_send.socket = socketTX;
					char portChar[10];
					snprintf(portChar, sizeof(portChar), "%d", port);
					DCCRequest(info_send.filename, mi_ip, portChar ,info_send.username);
					pthread_create( &th_dcc_sender, NULL, DCCSender,&info_send);	
					break;
				}
				case DOWNLOAD:
				{
					dcc_recv_info info_recv;
					info_recv = proc_dcc_download_line(param);
					pthread_create( &th_dcc_receiver, NULL, DCCReceiver,&info_recv);	
					break;
				}
				case MSG:
				{
					if(joined && (strcmp(channel,"")!=0))
					{
						privateMsg(channel,param);
						cout << "> " << msg << endl;
					}
					else
					{
						cout << "*** No has hecho JOIN a ningún canal, haz JOIN para poder enviar mensajes!" << endl;
					}
					break;	
				}
				case AUTH:
				{
					strcpy(paramNickname,param);
					nick(paramNickname);
					user();
					break;
				}
				case HELP:
				{
					cout << "##########################################################################" << endl;
					cout << "ircc: ayuda" << endl;
					cout << " 	Los comandos disponibles son:" << endl;
					cout << "		-/NICK <nickname> ..." << endl;
					cout << "			-ver RFC2812 (3.1.2)" << endl;
					cout << "		-/USER" << endl;
					cout << "			-ver RFC2812 (3.1.3)" << endl;
					cout << "		-/QUIT" << endl;
					cout << "			-ver RFC2812 (3.1.7)" << endl;
					cout << "		-/JOIN <#channel>" << endl;
					cout << "			-ver RFC2812 (3.2.1)" << endl;
					cout << "		-/PART <mensaje de salida>" << endl;
					cout << "			-ver RFC2812 (3.2.2)" << endl;
					cout << "		-/LIST <mask>" << endl;
					cout << "			-ver RFC2812 (3.2.6)" << endl;
					cout << "		-/WHO" << endl;
					cout << "			-ver RFC2812 (3.6.1)" << endl;
					cout << "		-/PING <ping param>" << endl;
					cout << "			-ver RFC2812 (3.7.2)" << endl;
					cout << "		-/PONG <pong param>" << endl;
					cout << "			-ver RFC2812 (3.7.3)" << endl;
					cout << "##########################################################################" << endl;	
					cout << "Transferencia de ficheros DCC:" << endl;
					cout << "	-Usuario A = enviante" << endl;
					cout << "	-Usuario B = receptor" << endl << endl;
					cout << "		A: /OFFER filename b" << endl;
					cout << "		B: Recibe la notificación del envío por parte de A" << endl;
					cout << "		B: /DOWNLOAD new_filename ip_de_A:DCCport" << endl;
					cout << "		-Se realiza la transferencia" << endl;					
					cout << "##########################################################################" << endl;
					break;
				}
				case NOP:
				{
					cout << "NOP - " << param << endl;
					break;
				}
				case SLEEP:
				{
					comando_sleep(param);
					break;
				}
				case CONNECT:
				{
					//sintaxis: /connect server:port
					//control del formato del parámetro (host:port)
					char* p;
					if(strcmp(param,"")!=0)
					{
						p = strtok(param,":");
						if(p!=NULL)
						{
							strcpy(host,p);
							p = strtok(NULL,":");
							if(p!=NULL)
							{
								port = atoi(p);
								irc_connect(host,port);
							}
							else
							{
								cout << "*** falta puerto!" << endl;
							}
						}
						else
						{
							cout << "*** falta host!" << endl;
						}
					}
					else
					{
						cout << "*** uso: /connect host:port" << endl;
					}
					break;
				}
				case DISCONNECT:
				{
					if(paramDepuracion)
					{
						cout << "DDD EJECUTAR COMANDO - DISCONNECT" << endl;						
						cout << "DDD PARAMS = " << param << endl;
					}
					quit(param);
					close(socketDesc);
					connected = 0;
					strcpy(channel,"");
					nickname = "";
					joined = 0;
					break;

				}
				default:
				{
					cout << "ERROR - Comando No reconocido" << endl;
					if(paramDepuracion)
					{
						cout << "DDD PARAMS = " << param << endl;
					}
					break;
				}
			}//switch
		}
		else
		{
			//la línea no es un comando
			//en caso de estar dentro de un canal se enviará ese mensaje
			if(joined && (strcmp(channel,"")!=0))
			{
				if(strcmp(msg,"")!=0)
				{
					privateMsg(channel,msg);
					cout << "> " << msg << endl;
				}				
			}
			else
			{
				cout << "*** No has hecho JOIN a ningún canal, haz JOIN para poder enviar mensajes!" << endl;
			}
		}
	}//FIN BUCLE PRINCIPAL DE LA APLICACIÓN
	return 0; 
}//FIN MAIN

//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
/*
   ###    ##     ## ##     ## #### ##       ####    ###    ########  ########  ######  
  ## ##   ##     ##  ##   ##   ##  ##        ##    ## ##   ##     ## ##       ##    ## 
 ##   ##  ##     ##   ## ##    ##  ##        ##   ##   ##  ##     ## ##       ##       
##     ## ##     ##    ###     ##  ##        ##  ##     ## ########  ######    ######  
######### ##     ##   ## ##    ##  ##        ##  ######### ##   ##   ##             ## 
##     ## ##     ##  ##   ##   ##  ##        ##  ##     ## ##    ##  ##       ##    ## 
##     ##  #######  ##     ## #### ######## #### ##     ## ##     ## ########  ######
*/
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------

//irc_connect: Función que realiza la conexión con un servidor IRC dados su host y el puerto.
void
irc_connect(string host,int port)
{
	if(host=="" || port==0)
	{
		error("ERROR - Datos de conexión mal formados!.");
	}
	socketDesc = socket(AF_INET,SOCK_STREAM,0);
	if(socketDesc < 0){
		error("ERROR - opening socket\n");
	}else{
		cout << "*** Socket Creado!\n";
	}	
	server = gethostbyname(host.data());
	if(server==NULL){
		error("ERROR - error en gethostbyname!\n");
	}	
	bzero((char *) &serverAddr, sizeof(serverAddr));
	serverAddr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, (char *)&serverAddr.sin_addr.s_addr, server->h_length);
	serverAddr.sin_port = htons(port);	
	//conectar socket al server
	if (connect(socketDesc,(struct sockaddr *) &serverAddr,sizeof(serverAddr)) < 0){ 
        	error("ERROR connecting");
	}else{
		cout << "*** Connect hecho!\n";
		int socklen = sizeof(struct sockaddr_in);
		getsockname(socketDesc,(struct sockaddr *)&my,(socklen_t*)&socklen);
		mi_ip = inet_ntoa(my.sin_addr);
	}
	connected = 1;
}

//sendMsg: Esta función recibe un mensaje por parámetros y la envia al server usando el socket conectado.
void
sendMsg(string msg)
{
	pthread_mutex_lock(&mutex_send);
	send(socketDesc,msg.data(),msg.length(),0);
	pthread_mutex_unlock(&mutex_send);
}

//error: Función empleada para el paso de errores
void
error(const char *err)
{
	perror(err);
	exit(0);
}

//isChannelMsg: Recibe una línea y determina si es o no un msenaje procedente de un canal
int
isChannelMsg(char *msg)
{
	if(strstr(msg,"PRIVMSG")!=NULL)
	{
		return 1;
	}	
	return 0;
}

//comando_sleep: Comando 'sleep <segundos>'
void
comando_sleep (string param)
{
	cout << "*** Sleep " << atoi(param.data()) << " segundos" << endl;
	sleep(atoi(param.data()));
}

//printResponse: Función que imprime por pantalla los mensajes recibidos desde el servidor
void
printResponse(char *msg)
{
	cout << "*** " << msg << endl;
}
//printChannelResponse: Función que imprime por pantalla los mensajes recibidos de un canal
void
printChannelResponse(char *msg)
{
	char msg_[200];
	char chan[100];
	char user[100];
	strcpy(msg_,"");
	strcpy(chan,"");
	strcpy(user,"");
	//BLOQUE PARA IMPRIMIR MSG DE CHANNEL	
	char *p;
	p = strtok(msg,":");
	p = strtok(NULL,"!");
	if(p!=NULL)
	{
		strcpy(msg_,p);		
	}
	//CHANNEL
	p = strtok(msg,"#");
	p = strtok(NULL,"#");
	if(p!=NULL)
	{
		strcpy(chan,p);
	}
	//USERNAME
	p = strtok(msg,"!");
	if(p!=NULL)
	{
		strcpy(user,p);
	}	
	cout << "< " << chan <<  user << " > " << msg_;
}


//proc_dcc_download_line: Esta función recibe los parámetros que se han dado al comando DOWNLOAD y los devuelve encapsulados en un Struct dcc_recv_info (para el thread receiver)
struct dcc_recv_info 
proc_dcc_download_line(char *line)
{
	///download num 192.168.1.141:8306 141661
	dcc_recv_info info;
	if(strcmp(line,"")!=0){
		string aux = line;
		string filename,ip,port;		
		//cout << "line [" << line << "]" <<endl<<endl;
		filename = aux.substr(0,aux.find(" "));
		//cout << "filename=" << filename << endl;
		aux = aux.substr(aux.find(" ")+1);
		ip = aux.substr(0,aux.find(":"));
		//cout << "ip=" << ip << endl;		
		//cout << "aux=" << aux << endl;
		aux = aux.substr(aux.find(":")+1);
		//cout << "aux=" << aux << endl;
		port = aux.substr(0,aux.find(" "));
		//cout << "port =" << port << endl;
		strcpy(info.filename,filename.data());
		strcpy(info.ip,ip.data());	
		strcpy(info.port,port.data());	
	}
	else
	{
		cout << "*** No se han recibido suficientes parámetros." << endl;
	}
	return info;
}

//proc_cdd_offer_line: Esta función recibe los parámetros que se han dado al comando OFFER y los devuelve encapsulados en un Struct dcc_send_info (para el thread DCCSEND)
struct dcc_send_info 
proc_dcc_offer_line(char *line)
{
	// el formato de "line" debe ser: "filename user"
	dcc_send_info info;
	if(strcmp(line,"")!=0){
		char *p;
		p = strtok(line, " ");
		//FILENAME
		if(p!=NULL)
		{
			strcpy(info.filename,p);	
			p = strtok(NULL, " ");
			//USERNAME
			if(p!=NULL)
			{
				strcpy(info.username,p);	
			}
		}
	}
	else
	{
		cout << "*** No se han recibido suficientes parámetros." << endl;
	}
	return info;
}

//isPing: función que determina si una línea recibida del server IRC es un PING
int
isPing(char *buffer)
{
	char *p;
	int ret = 0;
	//cout << "isPing.buffer=" << buffer <<	endl;
	if(strcmp(buffer,""))
	{	
		p = strtok(buffer," ");	
		if(strcmp(p,"PING")==0)
		{
			ret=1;
		}
	}
	return ret;
}

//isUserCommand: esta función recibe una línea de entrada del usuario, comprueba si comienza por '/' para evr si es un comando o un mensaje.
int
isUserCommand(char *line)
{
	int ret = 0;
	//cout << "isUserCommand.line=" << line << endl;
	if(line[0]=='/')
	{
		//comando de usuario
		ret = 1;
	}
	return ret;
}

//getUserCommand:  recibe una linea de CIN y devuelve el comando que la encabeza ejemplo: input== /JOIN #ro2 ; output== JOIN.
char*
getUserCommand(char *line)
{
	char *ret;
	ret =(char*) malloc(100*sizeof(char));
	if(strcmp(line,"")!=0)
	{	
		ret = strtok(line," /");
	}
	return ret;
}

//getParam: función que extrae el parametro que acompaña a un comando en las líneas recibidas desde teclado
char*
getParam(char *buffer)
{
	char *p;
	char *ret;
	ret =(char*) malloc(100*sizeof(char));
	if(strcmp(buffer,"")!=0)
	{	
		p = strtok(buffer," ");
		p = strtok(NULL," ");
		while(p!=NULL)
		{
			strcat(ret,p);
			strcat(ret," ");
			p = strtok(NULL," "); 
		}
	}
	return ret;
}

//getPingParam: función que extrae los parámetros de una línea de PING recibida del servidor IRC 
char*
getPingParam(char *buffer)
{
	char *p;
	char *ret;
	ret =(char*) malloc(100*sizeof(char));
	if(strcmp(buffer,"")!=0)
	{	
		p = strtok(buffer,":");
		p = strtok(NULL,":");
		strcpy(ret,p);
	}
	return ret;
}

//isRrqCorrecto: recibe un mensaje RRQ y el nombre del fichero ofrecido. comprueba el formato del RRQ y que los nombres de fichero son iguales.
int
isRrqCorrecto(string buffer, string filename)
{	
	/* 
	Formato correcto RRQ (RFC TFTP)
	  2 bytes     string    1 byte     string   1 byte
            ------------------------------------------------
           | Opcode |  Filename  |   0  |    Mode    |   0  |
            ------------------------------------------------
	*/
	int ret = 0;
	size_t pos;
	string resto = "";
	string opCode = "";
	string filenameRRQ = "";
	string mode = "";	
	opCode = buffer.substr(0,2);
	resto = buffer.substr (2,buffer.size()-2);
	pos = resto.find("0");    
	filenameRRQ = resto.substr(0,pos);
	resto = resto.substr(0,resto.size()-1);
	pos = resto.find("0");
	mode = resto.substr(pos+1,resto.size()-(pos+1)-2);
	/*
	Cosas a comprobar llegados a este punto:
		-Opcode == 01
		-filenameRRQ == filename
		-mode == NETASCII	
	*/
	if(opCode == "01" && filenameRRQ == filename && mode == "NETASCII")
	{
		ret=1;
		if(paramDepuracion){
			cout << "DDD RRQ CORRECTO" << endl;
		}
	}
	else if(paramDepuracion)
	{
		cout << "DDD RRQ INCORRECTO" << endl;
	}
	return ret;
}


//isACKCorrecto: recibe un mensaje ACK y el número de bytes enviados en el último paquete. comprueba si el ACK es correcto.
int
isACKCorrecto(char* ack)
{
	int ret = 0;
	if(strcmp(ack,"0x50")==0)
	{
		ret = 1;
	}
	return ret;
}	



//isDCCSend: función que comprueba si un PRIVMSG recibido es un DCC SEND
int
isDCCSend(char *msg)
{
	//Recibe una línea y determina si es o no un DCC Send
	// DCC ejemplo "DCC SEND file 192.168.1.129 9999"
	if(strstr(msg,"PRIVMSG")!=NULL && strstr(msg,"DCC SEND")!=NULL)
	{
		//extraer filename para el RRQ
		string linea = msg;
		string aux = linea.substr(linea.find("DCC SEND"),linea.size());
		aux = aux.substr(aux.find(" ")+1,aux.size());
		aux = aux.substr(aux.find(" ")+1,aux.size());
		string filename = aux.substr(0,aux.find(" "));
		//ya tenemos el filename del archivo original, lo guardamos para la petición RRQ
		DCCFilename = filename;
		return 1;
	}	
	return 0;
}


//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
/*
######## ##     ## ########  ########    ###    ########   ######  
   ##    ##     ## ##     ## ##         ## ##   ##     ## ##    ## 
   ##    ##     ## ##     ## ##        ##   ##  ##     ## ##       
   ##    ######### ########  ######   ##     ## ##     ##  ######  
   ##    ##     ## ##   ##   ##       ######### ##     ##       ## 
   ##    ##     ## ##    ##  ##       ##     ## ##     ## ##    ## 
   ##    ##     ## ##     ## ######## ##     ## ########   ######  
*/
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
void 
*receiver(void *arg)
{
	/*
		Este hilo se encarga de recibir todo, procesarlo, determinar de que tipo de datos se trata y enviarlo al lugar correspondiente
		Por defecto:
			-imprimir todo por pantalla
			-si se recibe un PING
				-leer el parámetro del PING y enviar un PONG con el mismo parámetro.
	*/
	cout << arg;
	while(true)
	{
		char buffer[1000] = "";
		char buffer_aux[1000] = "";	
		pthread_mutex_lock(&mutex_receive);
			recv(socketDesc,buffer,1000,0);
			strcpy(buffer_aux,buffer);
			/*
				Aquí vamos a comprobar se se ha recibido un "DCC SEND"
				en dicho caso avisaremos al usuario de la manera adecuada
				
				Hay que leer el msg que contiene el DCC SEND y extraer de ahí los datos que irán al struct de DCC (ip y puerto)
				el filename ya lo dará el usuario.
			*/
			if(isDCCSend(buffer_aux))
			{
				string a = buffer_aux;
				string sender,filename, ip, port,size;
				sender = a.substr(1,a.find("!")-1);			
				a = a.substr(a.find("SEND")+5);
				filename = a.substr(0,a.find(" "));
				a = a.substr(a.find(" ")+1);
				ip = a.substr(0,a.find(" "));
				a = a.substr(a.find(" ")+1);
				port = a.substr(0,a.find(" "));
				size = a.substr(a.find(" ")+1);
				//size = size.substr(size.find(" "),size.find("^A")-1);

				
				int u = 0;
				while(puertos_ips[u]!="" && u<500)
				{
					u++;
				}
				string puerto_ip = "";
				puerto_ip += port;
				puerto_ip += ":";
				puerto_ip += ip;
				puertos_ips[u] = puerto_ip;
				filenames[u] = filename;
				sizes[u] = atoi(size.data());

		
				cout << "*** El usuario <" << sender << "> te ofrece el fichero <" << filename << "> para descargar." << endl;
				cout << "*** El fichero tiene un tamaño de <" << atoi(size.data()) << "> bytes." << endl;
				cout << "*** para descargarlo usa el comando: /download " << filename << " " << ip << ":" << port << endl;								/*
				Aqui hay que extraer los parámetros del DCC SEND y mostrar un mensaje bien formateado:
				
					*** File offer from harry
					*** use "/download main.cc 127.0.0.1:53037" to download
					/download main.cc 127.0.0.1:53037
					*** Downloading file main.cc
					*** Download finished
				*/
			}
			else if(isPing(buffer_aux))
			{
				pong(getPingParam(buffer));
			}
			else if(strcmp(buffer_aux,"")!=0)
			{				
				if(isChannelMsg(buffer)==1)
				{
					printChannelResponse(buffer);
				}
				else
				{
					printResponse(buffer);
				}
			}
		pthread_mutex_unlock(&mutex_receive);
	}
	pthread_exit(0);
}

//DCCSender: código del thread destinado a enviar un fichero por DCC a otro cliente
void
*DCCSender(void *arg)
{
	cout << "*** Esperando la conexión del cliente"<< endl;
	dcc_send_info info;
	info = *((dcc_send_info*)(arg));
    	struct sockaddr_in Client_Address;	
	char buffer_rrq[256] = "";
	socklen_t fromlen;
	fromlen = sizeof Client_Address;
	recvfrom(info.socket, buffer_rrq, sizeof buffer_rrq, 0, (struct sockaddr *)&Client_Address, &fromlen);
	if(paramDepuracion)
	{
		printf("DDD Recibido paquete desde %s:%d\n\n\n",inet_ntoa(Client_Address.sin_addr), ntohs(Client_Address.sin_port));
	}
	string buffer_rrq_string = buffer_rrq;
	/*
	PROCESAR RRQ, Y SI ES CORRECTO ENVIAR PRIMER BLOQUE DEL FICHERO(DCC) Y ESPERAR ACK (DCC)	
	Para ser correcto el RRQ debe ser:	
		 01 |  Filename  |   0  |    Mode    |   0  		siendo Filename = el fichero enviado
	*/
	if(isRrqCorrecto(buffer_rrq_string,info.filename))
	{
		//RRQ CORECTO, este mensaje se muestra solo en modo Debug
		cout << "*** Se ha iniciado la transferencia del fichero <" << info.filename << ">" << endl;
	}
	// enviar fichero
	FILE *in = fopen(info.filename,"r");

	
	if(in)
	{
		char Buffer[5] = "";
	    	size_t len;
		char lastACK[5] = "0x50";
	    	memset (Buffer,'0',5);
		int fail = 0; 
		while ((len = fread(Buffer,sizeof(char),5, in)) > 0 && isACKCorrecto(lastACK) && fail==0 )
		{   
			memcpy(lastACK,"0x00",sizeof("0x00")); 
			//len almacena cuantos chars se han leido del fichero
			int sent = sendto(info.socket, Buffer, len, 0,(sockaddr*)&Client_Address, sizeof Client_Address);
			//recibir ACK, si temporizador caduca => error UDP LOSS!
			sent++;	
			//TEMPORIZADOR ACK
			fd_set fd; 
			FD_ZERO(&fd); 
			timeval tv; 
			tv.tv_sec = 5; 
			tv.tv_usec = 0;
			FD_SET(info.socket, &fd); 
			tv.tv_sec=0;
			select(1, &fd, NULL, NULL, &tv);
			tv.tv_sec=5;
			while (!FD_ISSET(info.socket, &fd) && fail==0) 
		        {
        			FD_SET(info.socket, &fd); 
		        	select(info.socket+1, &fd, NULL, NULL, &tv);
			        if (!FD_ISSET(info.socket, &fd))
				{
					fail=1;
					cout << "*** UDP LOSS DETECTED" << endl;
				}	
			}
			if (!FD_ISSET(info.socket, &fd))
			{
				break;
			}
			else
			{
			//FIN TEMPORIZADOR ACK
			//SE PUEDE RECIBIR EL ACK!
				if(fail==0)
				{	
					recvfrom(info.socket, lastACK, sizeof lastACK, 0, (struct sockaddr *)&Client_Address, &fromlen);
					if(paramDepuracion)
					{
						printf("DDD Recibido ACK desde %s:%d\n\n\n",inet_ntoa(Client_Address.sin_addr), ntohs(Client_Address.sin_port));
					}
				}
			}
		}
		cout << "*** Transferencia del fichero " << info.filename << " terminada!" << endl;

	}
	else
	{ //error fopen
		cout << "*** Fichero incorrecto <" << info.filename << ">" << endl;
	}
	//CLOSES
	fclose(in);
	close (info.socket);
    	pthread_exit(0);
}


//DCCReceiver: código del thread destinado a recibir un fichero por DCC desde otro cliente
void 
*DCCReceiver(void *arg)
{
	dcc_recv_info info;
	info = *((dcc_recv_info*)(arg));


	// crear socket UDP
 	int Socket;
	struct sockaddr_in Server_Address;  
    	Socket = socket ( AF_INET, SOCK_DGRAM, 0 );
    	if( Socket == -1 )
	{   
		error("ERROR - no se pudo crear un socket!");    
    	}
    	Server_Address.sin_family = AF_INET;
    	Server_Address.sin_port = htons (atoi(info.port));
    	Server_Address.sin_addr.s_addr = inet_addr(info.ip);
    	if( Server_Address.sin_addr.s_addr == INADDR_NONE )
	{
	        error( "ERROR - Bad Address!");
    	}
	//ENVIAR RRQ


	//buscar el filename asociado a este puerto

	string puerto_ip = "";
	puerto_ip += info.port;
	puerto_ip += ":";
	puerto_ip += info.ip;

	int j = 0;
	while( puertos_ips[j]!= puerto_ip && j<500){
		j++;
	}

	int filesize = sizes[j];

	string rrq = "01";
	rrq += filenames[j];
	rrq += "0NETASCII0\r\n";
	sendto(Socket, rrq.data(), rrq.size()+1, 0,(struct sockaddr*)&Server_Address, sizeof Server_Address);
	if(paramDepuracion)
	{
		cout << "DDD SEND RRQ" << endl;
	}
	// recibir fichero
	FILE *fp=fopen(info.filename,"w");
	//int count=0;
	char ack[5] = "0x50";
	socklen_t fromlen;
	fromlen = sizeof Server_Address;
	int len = 5;
	int fail = 0;
	int escritos = 0;
	cout << "*** Se ha iniciado la recepción del fichero <" << info.filename << ">" << endl;
        	while(escritos<filesize && fail==0)
		{
			char Buffer[5]="";
			if(paramDepuracion)
			{
				cout << "DDD WAITING FOR RECV" << endl; 
			}
			//TEMPORIZADOR RECEPCIÓN PAQUETE
			fd_set fd; //create a file descriptor set called fd
			FD_ZERO(&fd); //zero out our fd (otherwise this program tends to crash)
			timeval tv; //create a time value object
			tv.tv_sec = 3; //set our timer value to wait for 3 seconds
			tv.tv_usec = 0;
			FD_SET(Socket, &fd); //set our fd set so that it points to our UDP socket
			tv.tv_sec=0;
			select(1, &fd, NULL, NULL, &tv);
			tv.tv_sec=5;
			while (!FD_ISSET(Socket, &fd) && fail==0) //if socket hasn't received any data yet...
		        {
        			FD_SET(Socket, &fd); //re-set the fd so select() will work
		        	select(Socket+1, &fd, NULL, NULL, &tv);
			        if (!FD_ISSET(Socket, &fd))
				{
					fail=1;
					cout << "*** CLIENT UDP LOSS DETECTED" << endl;
				}	
			}
			if(fail==0)
			{
				len = recvfrom(Socket, Buffer, sizeof(Buffer), 0, (struct sockaddr *)&Server_Address, &fromlen);
				//SEND ACK
				sendto(Socket, ack, sizeof(ack), 0,(struct sockaddr*)&Server_Address, sizeof Server_Address);
				//para frenar la transferencia
				usleep(10000);
				//escribir a fichero
				fwrite(Buffer,sizeof(char),len, fp);
				escritos+=len;
				//cout << "Escritos=" << escritos << endl;
			}//if not fail
		}//while
	fclose(fp);
	close (Socket);
	if(fail==0)
	{
		cout << "*** Recepción del fichero " << info.filename << " terminada!" << endl;
	}
	else
	{
		cout << "*** Ocurrió un error durante la transferencia." << endl << "*** Transferencia Cancelada." << endl; 
	}
	pthread_exit(0);
}
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
/*
 ######   #######  ##     ##    ###    ##    ## ########   #######   ######  
##    ## ##     ## ###   ###   ## ##   ###   ## ##     ## ##     ## ##    ## 
##       ##     ## #### ####  ##   ##  ####  ## ##     ## ##     ## ##       
##       ##     ## ## ### ## ##     ## ## ## ## ##     ## ##     ##  ######  
##       ##     ## ##     ## ######### ##  #### ##     ## ##     ##       ## 
##    ## ##     ## ##     ## ##     ## ##   ### ##     ## ##     ## ##    ## 
 ######   #######  ##     ## ##     ## ##    ## ########   #######   ######  
*/
//---------------------------------------------------------------------------------
//---------------------------------------------------------------------------------
//join: Comando IRC JOIN
void
join(char *chan)
{
	//si no estamos JOINED se envia el msg de JOIN al server, si lo estamos -> err, debes salir de un canal para entrar en otro!!
	//si se envia el msg SET 'channel' y SET 'joined'
	if(joined!=1 && nickname!="" && connected==1)
	{
		//se envia el msg de JOIN a chan
		char msg[100];
		strcpy(msg,"JOIN ");
		strcat(msg,chan);
		strcat(msg,"\r\n");
		sendMsg(msg);
		//SET de las variables de estado
		joined=1;
		strcpy(channel,chan);		
	}
	else
	{
		if(strcmp(channel,"")!=0)
		{
			cout << "Error - Estas en el canal " << channel << ", debes salir antes de hacer JOIN a " << chan << endl;
		}
		else if(nickname=="")
		{
			cout << "*** Debes utilizar el comando 'auth' antes de hacer join." << endl;

		}
		else if(connected!=1)
		{
			cout << "*** Debes utilizar el comando 'connect' antes de hacer join." << endl; 

		}
		else
		{
			error("JOIN - Estado inconsistente de las variables joined y channel");	
		}
	}
}

//pong: función que contesta a un PING del server IRC
void
pong(char *param)
{
	char msg[100];
	strcpy(msg,"PONG ");
	if(strcmp(param,"")!=0)
	{
		strcat(msg,param);
	}
	strcat(msg,"\r\n");
	if(paramDepuracion)
	{
		cout <<"DDD COMANDO PONG - "<< msg << endl;
	}
	sendMsg(msg);
}

//ping: Comando IRC PING
void
ping(char *param)
{
	char msg[100];
	strcpy(msg,"PING ");
	if(strcmp(param,"")!=0)
	{
		strcat(msg,param);
	}
	strcat(msg,"\r\n");
	if(paramDepuracion)
	{
		cout <<"DDD COMANDO PING - "<< msg << endl;
	}
	sendMsg(msg);
}

//nick: comando IRC NICK
int
nick(const char *nick)
{
	if(paramDepuracion)
	{
		cout << "DDD NICK" << endl;
	}
	int ret = -1;
	//if(nickname=="" && connected==1)
	if(connected==1)
	{
		//el nick no ha sido fijado, se procede a fijarlo
		if(strcmp(nick,"")==0)
		{
			cout << "*** ERROR - NICK - no se ha proporcionado un nick\n" << endl;	
		}
		else
		{
			char str[100];
			strcpy(str,"NICK ");
			strcat(str,nick);
			strcat(str,"\r\n");
			//se envia el comando compuesto
			sendMsg(str);
			ret = 1;
		}
		nickname = nick;

	}
	else
	{
		//ya se había fijado el nick
		if(nickname!=""){
			cout << "*** Ya has registrado (auth) un nickname previamente. (" << nickname << ")" << endl;
		}
		if(connected!=1){
			cout << "*** Debes utilizar el comando 'connect' antes de registrar un nickname." << endl; 
		}

	}
	return ret;
}



//user: Comando IRC USER
void
user()
{
	if(paramDepuracion)
	{
		cout << "DDD USER" << endl;
	}
	sendMsg("USER anonymous unknown unknown :ircc\r\n");
}

//quit: Comando IRC QUIT
void
quit(char *str)
{
	char msg[100];
	strcpy(msg,"QUIT ");
	if(strcmp(str,"")!=0)
	{
		strcat(msg,":");
		strcat(msg,str);
	}
	strcat(msg,"\r\n");
	sendMsg(msg);
	if(paramDepuracion)
	{
		cout << msg << endl;
	}
}

//privateMsg: Función que envia un mensaje privado según el RFC de IRC
void
privateMsg(string receiver, string msg)
{
	//si estamos dentro de un canal 'joined' entonces escribimos a ese canal, si no -> error, "no estás JOINED a ningún canal"
	string str;
	str += "PRIVMSG ";
	str += receiver;	
	str += " :";	
	str += msg;	
	str += " \r\n";	
	if(paramDepuracion)
	{	
		cout << "DDD " << str << endl;	
	}
	sendMsg(str);
}

//who: Comando IRC WHO
void
who(char *param)
{
	//Se recibe un parámetro que representa la cadena de la máscara del comando WHO (ver RFC 3.6.1)
	char str[100];
	strcpy(str, "WHO ");
	strcat(str,param);
	strcat(str," \r\n");
	if(paramDepuracion)
	{
		cout << "DDD COMANDO WHO " << str << endl;
	}
	sendMsg(str);
}

//part: Comando IRC PART
void
part(char *leave_message)
{
	if(joined==1)
	{
		//estamos joined, luego podemos hacer PART
		if(strcmp(channel,"")!=0)
		{
			char msg[100];
			strcpy(msg,"PART ");
			if(paramDepuracion)
			{
				cout << "DDD CHANNEL " << channel << endl;
			}
			strcat(msg,channel);
			if(strcmp(leave_message,"")!=0)
			{
				strcat(msg," :");
				strcat(msg,leave_message);
			}
			strcat(msg,"\r\n");
			if(paramDepuracion)
			{
				cout << msg << endl;
			}
			sendMsg(msg);
		}
		else
		{
			error("PART - Estado inconsistente de las variables joined y channel");	
		}
	}
	else
	{
		//no estamos joined, luego err "not joined!"
		cout << "*** Error - No estás en ningún canal, no puedes hacer PART " << endl;
	}	
	strcpy(channel,"");
	joined = 0;
}

//list: Comando IRC LIST
void
list(char *channel)
{
	char msg[100];
	strcpy(msg,"LIST ");
	if(strcmp(channel,"")!=0)
	{
		strcat(msg,channel);
	}	
	strcat(msg,"\r\n");
	sendMsg(msg);
	if(paramDepuracion)
	{
		cout << msg << endl;
	}
}

//DCCRequest: Función para enviar oferta de transferencia de ficheros
void
DCCRequest(string filename, string ip, string port_,string receiver)
{
	/*
	Esta función se utiliza para hacer una petición de transferencia de archivos desde el SERVER al CLIENT
	la petición es una petición CTCP del tipo DCC

		DCC SEND <filename> <ip> <port>

	Esta petición se realiza sobre el IRC como un mensaje privado dirigido a un usuario conectado al server IRC.
	*/

	/*
	Se introducen los '^A' siguiendo las indicaciones del documento: http://www.user-com.undernet.org/documents/ctcpdcc.txt
	(ver sección: "Q: What about DCC?")
	*/
	struct stat filestatus;
	stat( filename.data(), &filestatus );


	char size[10];
	snprintf(size, sizeof(size), "%ld", (long int)filestatus.st_size);
	if(paramDepuracion)
	{
		cout << "Tamaño en bytes del fichero a enviar = " << size << endl;
	}

	string msg;
	msg += "DCC SEND ";
	msg += filename;
	msg += " ";
	msg += ip;
	msg += " ";
	msg += port_;
	msg += " ";
	msg += size;
	msg += "\r\n";	
	if(paramDepuracion)
	{
		cout << "DDD DCCRequest - [" << msg << "]" << endl;
	}	
	privateMsg(receiver, msg);
}
//---------------------------------------------------------------------------------

